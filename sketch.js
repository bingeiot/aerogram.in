const introSection = document.querySelector("section.intro")

let b1,b2;
const Y_AXIS = 1;
const X_AXIS = 2;
function setup() {
    const clientHeight = introSection.clientHeight;
	const clientWidth = introSection.clientWidth;

    const cnv = createCanvas(clientWidth, clientHeight);
    cnv.parent("intro")
    // background(0);
    b1 = color(255);
    b2 = color(0);
  }
  
  function draw() {
    // if (mouseIsPressed) {
    //   fill(0);
    // } else {
    //   fill(255);
    // }
    smooth();
    fill('rgba(255,255,255, 0.25)');
    noStroke()
    ellipse(Math.random()*width, Math.random()*height, 100, 100);

  }
