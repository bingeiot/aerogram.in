const mainImg = document.querySelector(".main-img");
const imgLists = document.querySelectorAll(".img-list img");

const removeAllSelectedClass = () => {
  imgLists.forEach(el => {
    el.classList.remove("selected");
  });
};

/**
 * Change main image on click
 */
imgLists.forEach(el => {
  el.addEventListener("click", e => {
    // console.log(e.target.src)
    mainImg.setAttribute("src", e.target.src);
    removeAllSelectedClass();
    el.classList.add("selected");
  });
});
